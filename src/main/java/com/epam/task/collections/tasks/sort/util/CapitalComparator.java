package com.epam.task.collections.tasks.sort.util;

import com.epam.task.collections.tasks.sort.TwoString;

import java.util.Comparator;

public class CapitalComparator implements Comparator<TwoString> {

    @Override
    public int compare(TwoString o1, TwoString o2) {
        return o1.getCapital().compareToIgnoreCase(o2.getCapital());
    }
}
