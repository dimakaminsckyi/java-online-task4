package com.epam.task.collections.tasks.sort;

public class TwoString implements Comparable<TwoString> {

    private String country;
    private String capital;

    public TwoString(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public String toString() {
        return "TwoString{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

    @Override
    public int compareTo(TwoString o) {
        return this.country.compareToIgnoreCase(o.getCountry());
    }
}
