package com.epam.task.collections.tasks.container;

import java.util.Arrays;

public class Container {

    private int counter = 0;
    private String[] stringArray = new String[5];

    public Container() {
    }

    public String[] getStringArray() {
        return stringArray;
    }

    public String getStringFromArray(int index) {
        return stringArray[index];
    }

    public void addStringToArray(String string) {
        if (counter == stringArray.length) {
            stringArray = Arrays.copyOf(stringArray, stringArray.length + 5);
        }
        stringArray[counter] = string;
        counter++;
    }

    public void showAllArray() {
        for (String c : stringArray) {
            System.out.println(c);
        }
    }
}
