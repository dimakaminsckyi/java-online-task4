package com.epam.task.collections.tasks.container;


import com.epam.task.collections.tasks.container.manager.AppManager;

import java.util.Scanner;

public class App {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        boolean flag = true;
        AppManager.compareFillResult();
        showMenu();
        while (flag) {
            switch (scanner.nextInt()) {
                case 1:
                    AppManager.compareFillResult();
                    break;
                case 2:
                    AppManager.compareGetResult();
                    break;
                case 0:
                    flag = false;
            }
        }
    }

    private static void showMenu(){
        System.out.println("\n" + "1 - Compare adding to data Structure");
        System.out.println("2 - Compare get from data structure");
        System.out.println("0 - Exit");
    }
}
