package com.epam.task.collections.tasks.container.manager;

import com.epam.task.collections.tasks.container.Container;

import java.util.ArrayList;
import java.util.List;

public class AppManager {

    private static Container container = new Container();
    private static List<String> stringList = new ArrayList<>();

    public static void compareFillResult() {
        long fillArrayTime = calculateDataFillArray();
        long fillListTime = calculateDataFillList();
        System.out.println("Method fill array in " + fillArrayTime + " nanosecond" + "\n" +
                "Method fill list in " + fillListTime + " nanosecond");
        if (fillArrayTime < fillListTime) {
            System.out.println("Array Data Structure faster fill 50 elements");
        } else {
            System.out.println("List Data Structure faster fill 50 elements" +
                    "(the size of the array is 5 at the beginning )");
        }
    }

    public static void compareGetResult() {
        long getArrayTime = calculateGetFromArray();
        long getListTime = calculateGetFromList();
        System.out.println("Method get array in " + getArrayTime + " nanosecond" + "\n" +
                "Method get list in " + getListTime + " nanosecond");
        if (getArrayTime < getListTime) {
            System.out.println("Array Data Structure faster get 50 elements");
        } else {
            System.out.println("List Data Structer faster get 50 elements");
        }
    }


    private static void fillData(Container container) {
        for (int i = 0; i < 50; i++) {
            container.addStringToArray("Example Array" + (i + 1));
        }
    }

    private static void fillData(List<String> stringList) {
        for (int i = 0; i < 50; i++) {
            stringList.add("Example List#" + (i + 1));
        }
    }

    private static long calculateDataFillArray() {
        long startTime = System.nanoTime();
        AppManager.fillData(container);
        return System.nanoTime() - startTime;
    }

    private static long calculateDataFillList() {
        long startTime = System.nanoTime();
        AppManager.fillData(stringList);
        return System.nanoTime() - startTime;
    }

    private static long calculateGetFromArray() {
        long startTime = System.nanoTime();
        container.showAllArray();
        return System.nanoTime() - startTime;
    }

    private static long calculateGetFromList() {
        long startTime = System.nanoTime();
        stringList.forEach(System.out::println);
        return System.nanoTime() - startTime;
    }
}
