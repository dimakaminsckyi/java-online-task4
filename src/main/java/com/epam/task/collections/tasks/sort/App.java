package com.epam.task.collections.tasks.sort;

import com.epam.task.collections.tasks.sort.util.TwoStringUtil;

public class App {

    public static void main(String[] args) {
        TwoStringUtil.generateRecords();
        TwoStringUtil.sortByCountry();
        TwoStringUtil.sortByCapital();
        TwoStringUtil.searchElement(new TwoString(null, "Kyiv"));

    }
}
