package com.epam.task.collections.tasks.sort.util;

import com.epam.task.collections.tasks.sort.TwoString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TwoStringUtil {

    private static List<TwoString> list = new ArrayList<>();
    private static CapitalComparator capitalComparator = new CapitalComparator();

    public static void generateRecords() {
        list.add(new TwoString("France", "Paris"));
        list.add(new TwoString("Ukraine", "Kyiv"));
        list.add(new TwoString("Spain", "Madrid"));
        list.add(new TwoString("Japan", "Tokyo"));
    }

    public static void sortByCountry() {
        Collections.sort(list);
        System.out.println("\n" + "sorted by country");
        showAllRecords();
    }

    public static void sortByCapital() {
        list.sort(capitalComparator);
        System.out.println("\n" + "sorted by capital");
        showAllRecords();
    }

    public static void searchElement(TwoString key) {
        int index = Collections.binarySearch(list, key, capitalComparator);
        System.out.println("\n" + list.get(index));
    }

    private static void showAllRecords() {
        list.forEach(System.out::println);
    }
}
