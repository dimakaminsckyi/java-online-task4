package com.epam.task.arrays.tasks.logical.util;

public final class ArrayUtil {

    public static int[] findSimilarElements(int[] array1, int[] array2) {
        int[] resultArray = new int[array1.length];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    resultArray[i] = array1[i];
                }
            }
        }
        return resultArray;
    }

    public static int[] findNotSimilarElement(int[] array1, int[] array2) {
        int[] resultArray = new int[array1.length];
        for (int i = 0; i < array1.length; i++) {
            int j;
            for (j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    break;
                }
            }
            if (j == array2.length) {
                resultArray[i] = array1[i];
            }
        }
        return resultArray;
    }

    public static void deleteSimilarElements(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int count = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    count++;
                }
                if (count == 3 && array[i] != 0) {
                    int repeat = array[i];
                    for (int e = 0; e < array.length; e++) {
                        if (repeat == array[e]) {
                            array[e] = 0;
                        }
                    }
                }
            }
        }
    }

    public static void deleteSequenceElement(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i != array.length - 1) {
                if (array[i] == array[i + 1]) {
                    array[i] = 0;
                }
            }
        }
    }
}