package com.epam.task.arrays.tasks.logical;

import com.epam.task.arrays.tasks.logical.util.Menu;

public class App {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.runMenu();
    }
}
