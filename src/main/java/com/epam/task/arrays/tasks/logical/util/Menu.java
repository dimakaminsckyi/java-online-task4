package com.epam.task.arrays.tasks.logical.util;

import java.util.Scanner;

public class Menu {

    private Scanner scanner = new Scanner(System.in);
    private int[] firstArray = {1, 2, 3, 4, 5, 6, 7, 8, 11, 15};
    private int[] secondArray = {1, 3, 5, 6, 7, 8, 9, 9, 10, 12};
    private int[] deleteArray = {1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7, 7};
    private int[] deleteSequenceArray = {1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 7};

    public void runMenu() {

        boolean flag = true;
        while (flag) {
            showMenu();
            switch (scanner.nextInt()) {
                case 1:
                    showResultSimilarElments();
                    break;
                case 2:
                    showResultNotSimilarElements();
                    break;
                case 3:
                    showResultDeleteSimilarElements();
                case 4:
                    showResultDeleteSequenceElements();
                    break;
                case 0:
                    flag = false;
            }
        }
    }

    public void showMenu() {
        System.out.println("\n" + "1 - Show Result of Similar Elements");
        System.out.println("2 - Show Result of Not Similar Elements");
        System.out.println("3 - Show Result of Delete Similar Elements");
        System.out.println("4 - Show Result of Delete Sequence Elements");
        System.out.println("0 - Exit");
    }

    private void showResultSimilarElments() {
        for (int i : ArrayUtil.findSimilarElements(firstArray, secondArray)) {
            if (i != 0) {
                System.out.print(i + " ");
            }
        }
    }

    private void showResultNotSimilarElements() {
        for (int i : ArrayUtil.findNotSimilarElement(firstArray, secondArray)) {
            if (i != 0) {
                System.out.print(i + " ");
            }
        }
    }

    private void showResultDeleteSimilarElements() {
        ArrayUtil.deleteSimilarElements(deleteArray);
        for (int i : deleteArray) {
            if (i != 0) {
                System.out.print(i + " ");
            }
        }
    }

    private void showResultDeleteSequenceElements() {
        ArrayUtil.deleteSequenceElement(deleteSequenceArray);
        for (int i : deleteSequenceArray) {
            if (i != 0) {
                System.out.print(i + " ");
            }
        }
    }
}
