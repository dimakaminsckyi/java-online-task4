package com.epam.task.arrays.tasks.game.util;

import com.epam.task.arrays.tasks.game.model.Artifact;
import com.epam.task.arrays.tasks.game.model.Door;
import com.epam.task.arrays.tasks.game.model.DoorType;
import com.epam.task.arrays.tasks.game.model.Monster;

public class GenerateRecord {

    public static Monster[] fillMonsterArray() {
        Monster[] monsters = new Monster[10];
        for (int i = 0; i < monsters.length; i++) {
            monsters[i] = new Monster();
        }
        return monsters;
    }

    public static Artifact[] fillArtifactArray() {
        Artifact[] artifacts = new Artifact[10];
        for (int i = 0; i < artifacts.length; i++) {
            artifacts[i] = new Artifact();
        }
        return artifacts;
    }

    public static Door[] fillDoorArray(Monster[] monsters, Artifact[] artifacts) {
        Door[] doors = new Door[10];
        int id = 1;
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door(id);
            id++;
            if (doors[i].getDoorType() == DoorType.ARTIFACT) {
                doors[i].setArtifacts(artifacts[Randomizer.getRandomNumberInRange(0, 9)]);
            } else {
                doors[i].setMonsters(monsters[Randomizer.getRandomNumberInRange(0, 9)]);
            }
        }
        return doors;
    }
}
