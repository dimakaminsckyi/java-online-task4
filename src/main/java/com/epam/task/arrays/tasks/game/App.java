package com.epam.task.arrays.tasks.game;

import com.epam.task.arrays.tasks.game.util.Menu;


public class App {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.runMenu();
    }
}
