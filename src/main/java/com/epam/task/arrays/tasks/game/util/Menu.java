package com.epam.task.arrays.tasks.game.util;

import com.epam.task.arrays.tasks.game.exception.EmptyDoorException;
import com.epam.task.arrays.tasks.game.model.Door;
import com.epam.task.arrays.tasks.game.model.Hall;
import com.epam.task.arrays.tasks.game.util.impl.GameUtilImpl;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;
    private GameUtil gameUtil;
    private Hall hall;

    public Menu() {
        scanner = new Scanner(System.in);
        gameUtil = new GameUtilImpl();
        hall = new Hall();
    }

    public void runMenu() {
        boolean flag = true;
        while ((gameUtil.isGameContinue()) && (flag)) {
            showMenu();
            switch (scanner.nextInt()) {
                case 1:
                    enterDoorMenu();
                    break;
                case 2:
                    showStatisticTable();
                    break;
                case 3:
                    showDoors();
                    break;
                case 4:
                    showCheatMenu();
                    cheatMenu();
                    break;
                case 5:
                    flag = false;
            }
        }
    }

    private void showMenu() {
        System.out.println("\n" + "1 - Enter the door ");
        System.out.println("2 - Show Statistic ");
        System.out.println("3 - Show Doors");
        System.out.println("4 - Cheat Menu!");
        System.out.println("5 - Exit" + "\n");
    }

    private void enterDoorMenu() {
        System.out.println("Enter door number");
        int doorNumber = scanner.nextInt();
        try {
            gameUtil.enterDoor(hall, hall.getDoorById(doorNumber));
        } catch (EmptyDoorException e) {
            System.out.println(e.getMessage() + "\n");
        }
        hall.clearDoor(doorNumber - 1);
    }

    private void cheatMenu() {
        switch (scanner.nextInt()) {
            case 1:
                System.out.println(gameUtil.countDoorsOfDeath(hall) + " Doors");
                break;
            case 2:
                gameUtil.orderWiningDoors(hall);
                break;
            case 3:
                break;
        }
    }

    private void showCheatMenu() {
        System.out.println("1 - How much doors does waiting for your death?");
        System.out.println("2 - Order of winning doors");
        System.out.println("3 - Return\n");
    }

    private void showStatisticTable() {
        System.out.println("-----------------------------------------");
        System.out.printf("%15s %20s", "HERO STRENGTH", "REMAINS DOORS");
        System.out.println();
        System.out.println("-----------------------------------------");

        System.out.format("%10s %20s",
                hall.getHero().getStrength(), gameUtil.getDoorRemains());
        System.out.println();
        System.out.println("-----------------------------------------");
    }

    private void showDoors() {
        System.out.println("-----------------------");
        System.out.printf("%15s", "DOORS_ID");
        System.out.println();
        System.out.println("-----------------------");
        for (Door d : hall.getDoors()) {
            if (d != null) {

                System.out.format("%10s",
                        d.getId());
                System.out.println();
            }
        }
        System.out.println("-----------------------");
    }
}
