package com.epam.task.arrays.tasks.game.model;

import com.epam.task.arrays.tasks.game.util.Randomizer;

public class Door {

    private int id;
    private Artifact artifact;
    private Monster monster;
    private DoorType doorType;

    public Door(int id) {
        this.id = id;
        this.doorType = Randomizer.getRandomDoorType();
    }

    public int getId() {
        return id;
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public Monster getMonster() {
        return monster;
    }

    public DoorType getDoorType() {
        return doorType;
    }

    public void setArtifacts(Artifact artifact) {
        this.artifact = artifact;
    }

    public void setMonsters(Monster monster) {
        this.monster = monster;
    }

    @Override
    public String toString() {
        return "Door{" +
                "id= " + id +
                "artifacts=" + artifact +
                ", monsters=" + monster +
                ", doorType=" + doorType +
                '}';
    }
}
