package com.epam.task.arrays.tasks.game.util;

import com.epam.task.arrays.tasks.game.model.DoorType;

import java.util.Random;

public class Randomizer {

    private static Random random = new Random();

    public static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        return random.nextInt((max - min) + 1) + min;
    }

    public static DoorType getRandomDoorType() {
        int random = (int) Math.round(Math.random());
        if (random == 1) {
            return DoorType.ARTIFACT;
        } else {
            return DoorType.MONSTER;
        }
    }
}
