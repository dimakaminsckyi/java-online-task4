package com.epam.task.arrays.tasks.game.model;

public enum DoorType {
    MONSTER,
    ARTIFACT
}
