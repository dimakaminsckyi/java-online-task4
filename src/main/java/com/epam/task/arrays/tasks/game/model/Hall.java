package com.epam.task.arrays.tasks.game.model;

import com.epam.task.arrays.tasks.game.util.GenerateRecord;

public class Hall {

    private Door[] doors;
    private Hero hero;

    public Hall() {
        doors = GenerateRecord.fillDoorArray(GenerateRecord.fillMonsterArray(), GenerateRecord.fillArtifactArray());
        hero = new Hero();
    }

    public Door[] getDoors() {
        return doors;
    }

    public Hero getHero() {
        return hero;
    }

    public Door getDoorById(int id){
        return doors[id - 1];
    }

    public void clearDoor(int index){
        doors[index] = null;
    }
}
