package com.epam.task.arrays.tasks.game.util.impl;

import com.epam.task.arrays.tasks.game.exception.EmptyDoorException;
import com.epam.task.arrays.tasks.game.model.*;
import com.epam.task.arrays.tasks.game.util.GameUtil;

public class GameUtilImpl implements GameUtil {

    private int doorRemains = 10;

    @Override
    public void increaseHeroStrength(Hall hall, Artifact artifact) {
        hall.getHero().addStrength(artifact);
        doorRemains--;
        System.out.println("Hero improve own strength on " + artifact.getStrength());
    }

    @Override
    public void fightWithMonster(Hall hall, Monster monster) {
        int heroStrength = hall.getHero().getStrength();
        System.out.println("Monster strength - " + monster.getStrength()
                + "\nYour strength - " + heroStrength);
        if (heroStrength >= monster.getStrength()) {
            doorRemains--;
            System.out.println("You had defeat the monster!");
        } else {
            doorRemains = -1;
            System.out.println("You have lose please try again ");
        }
    }

    @Override
    public void enterDoor(Hall hall, Door door) throws EmptyDoorException {
        if (door == null) {
            throw new EmptyDoorException("Please choose another door");
        }
        switch (door.getDoorType()) {
            case ARTIFACT:
                System.out.println("You found the artifact");
                increaseHeroStrength(hall, door.getArtifact());
                break;
            case MONSTER:
                System.out.println("You opened the door with the monster");
                fightWithMonster(hall, door.getMonster());
                break;
        }
    }

    @Override
    public boolean isGameContinue() {
        if (doorRemains == 0) {
            System.out.println("Congratulation , you have won the game!");
            return false;
        } else if (doorRemains < 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int getDoorRemains() {
        return doorRemains;
    }

    @Override
    public int countDoorsOfDeath(Hall hall) {
        int count = 0;
        for (Door d : hall.getDoors()) {
            if (d == null) {
                continue;
            }
            if ((d.getDoorType() == DoorType.MONSTER) &&
                    d.getMonster().getStrength() > hall.getHero().getStrength()) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void orderWiningDoors(Hall hall) {
        int heroLocalStrength;
        int highestMonsterStrength = 0;
        for (Door d : hall.getDoors()) {
            if (d != null) {
                if (d.getDoorType() == DoorType.MONSTER && d.getMonster().getStrength() > highestMonsterStrength) {
                    highestMonsterStrength += d.getMonster().getStrength();
                }
            }
        }
        heroLocalStrength = getHeroMaxStrengthInGame(hall);
        if (heroLocalStrength < highestMonsterStrength) {
            System.out.println("Sorry this game cannot be this game cannot be won\n");
        } else {
            printMonsterDoorOrder(hall);
        }
    }

    private int getHeroMaxStrengthInGame(Hall hall) {
        int heroLocalStrength = hall.getHero().getStrength();
        for (Door d : hall.getDoors()) {
            if (d != null) {
                if (d.getDoorType() == DoorType.ARTIFACT) {
                    heroLocalStrength += d.getArtifact().getStrength();
                    System.out.print(d.getId() + " ");
                }
            }
        }
        return heroLocalStrength;
    }

    private void printMonsterDoorOrder(Hall hall) {
        for (Door d : hall.getDoors()) {
            if (d != null) {
                if (d.getDoorType() == DoorType.MONSTER) {
                    System.out.print(d.getId() + " ");
                }
            }
        }
    }
}
