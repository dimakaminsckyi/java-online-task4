package com.epam.task.arrays.tasks.game.model;

import com.epam.task.arrays.tasks.game.util.Randomizer;


public class Artifact {

    private int strength;

    public Artifact() {
        this.strength = Randomizer.getRandomNumberInRange(10, 80);
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "strength=" + strength +
                '}';
    }
}
