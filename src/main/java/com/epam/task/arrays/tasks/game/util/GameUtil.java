package com.epam.task.arrays.tasks.game.util;

import com.epam.task.arrays.tasks.game.exception.EmptyDoorException;
import com.epam.task.arrays.tasks.game.model.*;

public interface GameUtil {

    void increaseHeroStrength(Hall hall, Artifact artifact);

    void fightWithMonster(Hall hall, Monster monster);

    void enterDoor(Hall hall, Door door) throws EmptyDoorException;

    boolean isGameContinue();

    int getDoorRemains();

    int countDoorsOfDeath(Hall hall);

    void orderWiningDoors(Hall hall);
}
