package com.epam.task.arrays.tasks.game.model;

import com.epam.task.arrays.tasks.game.util.Randomizer;


public class Monster {

    private int strength;

    public Monster() {
        this.strength = Randomizer.getRandomNumberInRange(5, 100);
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "strength=" + strength +
                '}';
    }
}
