package com.epam.task.arrays.tasks.game.exception;

public class EmptyDoorException extends Exception {

    public EmptyDoorException() {
    }

    public EmptyDoorException(String message) {
        super(message);
    }
}
