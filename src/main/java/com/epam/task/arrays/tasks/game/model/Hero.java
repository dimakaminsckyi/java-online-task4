package com.epam.task.arrays.tasks.game.model;

public class Hero {

    private int strength = 25;

    public Hero() {
    }

    public void addStrength(Artifact artifact) {
        this.strength += artifact.getStrength();
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "strength=" + strength +
                '}';
    }
}
